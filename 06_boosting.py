import pandas as pd

from sklearn.ensemble import GradientBoostingClassifier

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

if __name__ == "__main__":

    dt_heart = pd.read_csv('data/heart.csv' )
    print(dt_heart['target'].describe())


    #min 0, max 1
    X = dt_heart.drop(['target'], axis=1)
    y = dt_heart['target']

    #tamano de conjunto de Test de 35%
    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.35)


    #Comparador de clasificador con ensamble. 
    #Arboles de decisión 
            #  n_estimators = numero de arboles que vamos a usar
    boost = GradientBoostingClassifier(n_estimators=50).fit(x_train, y_train)
    boost_pred = boost.predict(x_test)
    print('='*64)
    print('Accuracy Bosting with GradientBoosting:', accuracy_score(boost_pred, y_test))
    print('='*64)


