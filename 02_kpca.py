# Importamos las bibliotecas generales
 
import pandas as pd
import sklearn
import matplotlib.pyplot as plt 
 
# Importamos los módulos específicos
 
from sklearn.decomposition import KernelPCA
 
from sklearn.linear_model import LogisticRegression
 
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
 
if __name__ == "__main__":
 
   # Cargamos los datos del dataframe de pandas
   dt_heart = pd.read_csv('data/heart.csv')
 
   # Imprimimos un encabezado con los primeros 5 registros
   print(dt_heart.head(5))
 
   # Guardamos nuestro dataset sin la columna de target
   dt_features = dt_heart.drop(['target'], axis=1)
   # Este será nuestro dataset, pero sin la columna
   dt_target = dt_heart['target']
 
   # Normalizamos los datos. Carga los datos, ajusta los modelos y aplica transformacion 
   dt_features = StandardScaler().fit_transform(dt_features)
  
   # Partimos el conjunto de entrenamiento. Para añadir replicabilidad usamos el random state
        #test_size -> tamaño del conjunto de prueba , random_state-> mientras sea el mismo valor, nos entrega el mismo valor
   X_train, X_test, y_train, y_test = train_test_split(dt_features, dt_target, test_size=0.3, random_state=42)

   print(X_train.shape)
   print(y_train.shape)


   # Llamamos y configuramos nuestro algoritmo KERNELpca
   '''EL número de componentes es opcional, ya que por defecto si no le pasamos el número de componentes lo asignará de esta forma:
   a: n_components = min(n_muestras, n_features)'''
   # kernel = 'linear'  ó   kernel = 'poly'   ó    kernel = 'rbf'
   kpca = KernelPCA(n_components= 4 , kernel = 'poly')
   kpca.fit(X_train)


   # Configuramos los datos de entrenamiento y test, de acuerdo a PCA
   dt_train = kpca.transform(X_train)
   dt_test = kpca.transform(X_test)



   #Configurar la regresion logistica
   logistic = LogisticRegression(solver='lbfgs')
   
   # Mandamos los data frames la la regresión logística
   logistic.fit(dt_train, y_train)
   
   #Calculamos nuestra exactitud de nuestra predicción
   print("SCORE KPCA: ", logistic.score(dt_test, y_test))
   