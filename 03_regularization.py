# Importamos las bibliotecas generales
 
import pandas as pd
import sklearn
import matplotlib.pyplot as plt 
 
# Importamos los módulos específicos
 
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Lasso
from sklearn.linear_model import Ridge
 
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
 
if __name__ == "__main__":
 
   # Cargamos los datos del dataframe de pandas
   datasset = pd.read_csv('./data/whr2017.csv')
 
   # Imprimimos un encabezado con los primeros 5 registros
   print(datasset.describe())
   print(datasset.columns)

   #Escogemos nuestros features y targets
     #Feature
   X = datasset[['gdp', 'family', 'lifexp',
       'freedom', 'generosity', 'corruption', 'dystopia']]
     #Target
   y = datasset[['score']]

   print(X.shape)
   print(y.shape) 

   #Partir el datasset en trait y test 
   X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)


   #Definir los regresores. 

   #Definir modelo lineal
   modelLinear = LinearRegression().fit(X_train,y_train)
   #Calcular la prediccion 
   y_predict_linear = modelLinear.predict(X_test)

   #Definir modelo Lasso, entre mayor el alpha mayor es la penalizacion para los features
   modelLasso = Lasso(alpha=0.02).fit(X_train,y_train)
   #Calcular la prediccion 
   y_predict_lasso = modelLasso.predict(X_test)

   #Definir modelo Ridge, entre mayor el alpha mayor es la penalizacion para los features
   modelRidge = Ridge(alpha=1).fit(X_train,y_train)
   #Calcular la prediccion 
   y_predict_ridge = modelRidge.predict(X_test)


   #Calcular metrica para calcular el error - perdida para cada modelo. 
   
   #ModeloLineal
   linear_loss = mean_squared_error(y_test, y_predict_linear)
   print('Linear Loss: ', linear_loss)

   #Modelo Lasso
   lasso_loss = mean_squared_error(y_test, y_predict_lasso)
   print('Lasso Loss: ', lasso_loss)

   #Modelo Lasso
   ridge_loss = mean_squared_error(y_test, y_predict_ridge)
   print('Ridge Loss: ', ridge_loss)


   #Ver en tiempo real como los coeficiente alpha altera a los features 

   print('='*32)
   print('Coeficientes Lasso')
   print(modelLasso.coef_)

   print('='*32)
   print('Coeficientes Ridge')
   print(modelRidge.coef_)