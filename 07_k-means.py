import pandas as pd

from sklearn.cluster import MiniBatchKMeans

if __name__ == "__main__":

    dataset = pd.read_csv('./data/candy.csv')
    print(dataset.head())
    
    #No se divide el dataset en conjunto de train y test, 
    # dado que es aprendizaje no supervizado

    #Guardamos el dataframe sin la columna que contiene los nombres
    X = dataset.drop('competitorname', axis=1)

    
    #Seleccionamos el modelo 
        # n_clusters => Número de grupos  
        # batch_size => Agrupamiento de datos, de que tamaño van a ser los grupos que van a pasar por el entrenamiento 
    kmeans = MiniBatchKMeans(n_clusters= 4 , batch_size=8).fit(X)

    print('Total de centros: ', len(kmeans.cluster_centers_))
    print('='*64)
    print(kmeans.predict(X))

    dataset['group'] = kmeans.predict(X)

    print(dataset)

